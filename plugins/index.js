import { 
    Swipe, SwipeItem ,
    Image as VanImage,
    Button,
    Popup,
    Dialog
} from 'vant';

let components = [
    Swipe, SwipeItem,VanImage,Button,Popup,
    Dialog
]
export function pluginVant(app) {
    components.forEach(i => {
        app.use(i)
    })
}
