import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import store from '@/stores/store.js'
import { showDialog } from 'vant';


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/musicitem',
      name: 'musicitem',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/MusicItem.vue')
    },
    {
      path: '/search',
      name: 'search',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/SearchView.vue')
    },
    {
      path: '/login',
      name: 'login',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/Login.vue')
    },
    {
      path: '/user',
      name: 'user',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/InfoUser.vue')
    }
  ]
})
router.beforeEach((to, from) => {
  // 判断登录
  if (to.name == 'user' || to.name == 'login') {
    if (!localStorage.getItem('token')) {
      showDialog({ message: '请先登录' })
      if (to.name == 'user') {
        return { name: 'login' }
      }
    } else {
      // 取出localstorage用户数据
      store.commit('updateProfile', JSON.parse(localStorage.getItem('user')))
      // token,cookie
      store.commit('updateToken', localStorage.getItem('token'))
      store.commit('updateCookie', localStorage.getItem('cookie'))
      return true
    }
  } else {
    
  }
})

export default router
