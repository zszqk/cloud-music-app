import service from "./axios";
// 手机号登录
export const loginByPhone = (data)=>service.post(`/login/cellphone`,data)
// 获取验证码
export const getCaptcha = (phone)=>service.get(`/captcha/sent?phone=${phone}`)
// 验证码验证（注册时需要提供被验证过的验证码）
export const verifyCaptcha = (data)=>service.post(`/captcha/verify`,data)
// 检测手机号码是否已注册（该接口会直接返回已注册手机号的信息）
export const checkPhone = (phone)=>service.get(`/cellphone/existence/check?phone=${phone}`)