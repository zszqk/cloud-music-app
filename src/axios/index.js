import service from './axios'

// 获取首页轮播图
export const getBanners = ()=>service.get("/banner?type=2")

// 获取首页推荐歌单数据
export const getGDplaylist = ()=>service.get(`/personalized?limit=10`)

// 获取歌单详情页
export const getMusicItem = (id)=>service.get(`/playlist/detail?id=${id}`)
// 获取详情页歌曲列表ids为逗号分隔的id
export const getItemList = (ids)=>service.get(`/song/detail?ids=${ids}`)
// 获取音乐URl
export const getSongUrl = (ids)=>{
    if(Array.isArray(ids)) ids = ids.join(',')
    return service.get(`/song/url?id=${ids}`)
}
// 获取歌词
export const getMusicLyric = (id)=>service.get(`/lyric?id=${id}`)
// 搜索
export const getSearchResult = (keyword)=>service.get(`/search?keywords= ${keyword}`)

// 获取用户详情
export const getProfileDetail = (uid)=>service.get(`/user/detail?uid=${uid}`)