// 数字转换
export function formatNumber(num){
    if (num > 1e10) return (num / 1e10).toFixed(1) + "亿"
    if (num > 1e4) return (num / 1e4).toFixed(1) + "万"
    return num
}