import { createStore } from 'vuex'
import { getMusicLyric,getProfileDetail } from '../axios'
const store = createStore({
    state() {
        return {
            playList: [
                {
                    al: {
                        id: 39098,
                        name: "降临",
                        pic: 109951163077752480,
                        picUrl: "https://p2.music.126.net/CflH8LlraC4zCbunsjAG3Q==/109951163077752483.jpg",
                        pic_str: "109951163077752483"
                    }
                    ,
                    ar:[
                        {id:13584,name:"至上励合"}
                    ],
                    id: 394748,
                    name: "降临"
                }
            ],
            playIndex: 0,
            isPause: true,
            lyric:{},//歌词,
            currentTime:0,//当前时间
            duration:0,//持续时间，
            isPlaySong:true, //是否显示底部播放组件
            profile:{},//用户信息
            token:'',
            cookie:''
        }
    },
    mutations: {
        updateIsPause(state, value) {
            state.isPause = value
        },
        updatePlayIndex(state, value) {
            state.playIndex = value
        },
        updatePlayList(state, value) {
            state.playList = value
        },
        updateLyric(state,value){
            state.lyric = value
        },
        updateTime(state,value){
            state.currentTime = value
        },
        updateDuration(state,value){
            state.duration = value
        },
        updateisPlaySong(state,value){
            state.isPlaySong = value
        },
        // 用户信息
        updateProfile(state,value){
            state.profile = value
        },
        updateToken(state,value){
            state.token = value
        },
        updateCookie(state,value){
            state.cookie = value
        }
        
    },
    actions:{
        async getLyric(context,id){
            let res = await getMusicLyric(id)
            // console.log(res);
            context.commit('updateLyric',res.data.lrc.lyric)
        },
        // 获取用户详情，持久化存储
        async getProfileDetail(context,uid){
            console.log(uid);
            let res = await getProfileDetail(uid)
            // 用户详情持久化存储
            localStorage.setItem('user',JSON.stringify(res.data))
            console.log(res);
            context.commit('updateProfile',res.data)
            
            
        }
    }
    
})
export default store
