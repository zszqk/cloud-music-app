import './assets/main.css'
import 'vant/lib/index.css';
import { createApp } from 'vue'
import store from './stores/store';
import App from './App.vue'
import router from './router'
import { pluginVant } from '../plugins'
const app = createApp(App)
// 加载vant组件
pluginVant(app)
app.use(store)
app.use(router)

app.mount('#app')
