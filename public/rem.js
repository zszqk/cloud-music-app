function remSize(){
    // 获取设备宽度
    let deviceWidth = document.documentElement.clientWidth || window.innerWidth
    if(deviceWidth>=750) deviceWidth = 750
    if(deviceWidth<=320) deviceWidth = 320
    // 1rem = 100px
    document.documentElement.style.fontSize = deviceWidth/7.5 + 'px';
    // 设置字体大小 切记此处将该文件放在body最下面，避免因为文档还未加载完毕js无法获取对应元素
    window.document.body.style.fontSize = 0.3+'rem'
}
remSize()
window.addEventListener("resize",()=>{
    remSize()
})